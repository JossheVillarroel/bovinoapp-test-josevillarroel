var express = require("express"),
    app = express(),
    bodyParser = require("body-parser"),
    methodOverride = require("method-override");
mongoose = require('mongoose');

app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());
app.use(methodOverride());

var router = express.Router();

router.post('/rut', function (req, res) {
    var textEncoded = '';
    var textToEncode = '';

    if (req.body.rut !== '') {
        const crypto = require('crypto');
        // llave
        let keyString = 'ionix123456';
        // texto a encriptar
        textToEncode = req.body.rut;
        // encriptacion de texto
        var key = new Buffer(keyString.substring(0, 8), 'utf8');
        var cipher = crypto.createCipheriv('des-ecb', key, '');
        var c = cipher.update(textToEncode, 'utf8', 'base64');
        textEncoded += cipher.final('base64');

        const axios = require('axios');

        var startTime = new Date();
        axios.get("http://bovinoapp.com/test/search?rut=" + textEncoded)
            .then(response => {
                // tiempo de respuesta
                endTime = new Date();
                var timeDiff = endTime - startTime;
                // modificamos la respuesta segun test tecnico
                response.data.result.registerCount = response.data.result.items.length;
                response.data.elapsedTime = timeDiff;
                // devolvemos un json con la data solicitada
                res.json({
                    search: response.data,
                });
            })
            .catch(error => {
                console.log(error);
            });
    } else {
        res.json("Error al obtener el rut por POST");
    }
});

app.use(router);

app.listen(3000, function () {
    console.log("Node server running on http://localhost:3000");
});